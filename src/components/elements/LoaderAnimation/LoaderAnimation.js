import React, {Component, PropTypes} from 'react';
import CSSModules from 'react-css-modules';

import styles from './LoaderAnimation.sss';


@CSSModules(styles, {allowMultiple: true})
export default class LoaderAnimation extends Component {
  render() {
    return (
      <div styleName="loader">
        <div styleName="loader-inner"></div>
      </div>
    );
  }
}
