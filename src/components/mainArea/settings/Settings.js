import React, {Component, PropTypes} from 'react';
import CSSModules from 'react-css-modules';
import {Parse} from 'parse';

import InputControl from 'components/elements/InputControl/InputControl';
import ButtonControl from 'components/elements/ButtonControl/ButtonControl';
import ContainerComponent from 'components/elements/ContainerComponent/ContainerComponent';
import LoaderAnimation from 'components/elements/LoaderAnimation/LoaderAnimation';
import {checkSiteName, checkSiteURL} from 'utils/data';
import {DEPLOY_RESULT_OK, DEPLOY_RESULT_ERROR} from 'ducks/content';

import styles from './Settings.sss';


const ERROR_BLANK_NAME  = "The name is required!";
const ERROR_WRONG_URL   = "The domain URL is wrong!";
const ERROR_NAME_EXIST  = "This name is already exists";
const ERROR_URL_EXIST   = "This domain URL is already exists";

const MSG_DEPLOY_COMPLETE = "Deploy succesfully completed!";
const MSG_DEPLOY_ERROR    = "An error during deploy!";


@CSSModules(styles, {allowMultiple: true})
export default class Settings extends Component {
  state = {
    name: '',
    domainURL: '',
    dirty: false,

    error: null,

    isDeploying: false,
    deployResult: null
  };
  site = null;

  componentWillMount() {
    this.site = this.props.site;
    this.setState({
      name: this.site.name,
      domainURL: this.site.domainURL,
      deployResult: this.props.deployResult,
      isDeploying: this.props.isDeploying
    });
  }

  componentWillReceiveProps(nextProps) {
    this.site = nextProps.site;
    this.setState({
      name: this.site.name,
      domainURL: this.site.domainURL,
      isDeploying: nextProps.isDeploying,
      deployResult: nextProps.deployResult
    });
  }

  onChangeName = event => {
    let name = event.target.value;
    this.setState({name, dirty: true, error: null});
  };

  onChangeDomain = event => {
    let domainURL = event.target.value;
    this.setState({domainURL, dirty: true, error: null});
  };

  validate() {
    if (!this.state.name) {
      this.setState({error: ERROR_BLANK_NAME});
      return false;
    }

    let pattern = new RegExp('^(https?:\\/\\/)' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    if (!pattern.test(this.state.domainURL)) {
      this.setState({error: ERROR_WRONG_URL});
      return false;
    }

    if (!checkSiteName(this.state.name, this.site)) {
      this.setState({error: ERROR_NAME_EXIST});
      return false;
    }

    if (!checkSiteURL(this.state.domainURL, this.site)) {
      this.setState({error: ERROR_URL_EXIST});
      return false;
    }

    this.setState({error: null});
    return true;
  }

  onSave = e => {
    e.preventDefault();
    if (this.validate()) {
      this.setState({dirty: false});
      this.site.name = this.state.name;
      this.site.domainURL = this.state.domainURL;
      this.props.updateSite(this.site);
    }
  };

  onDeploy = () => {
    this.props.deploy(this.site);
  };

  onWizard = () => {
    window.location.href = "/wizard.html?id=" + this.site.origin.id;
  };

  render() {
    const {isEditable} = this.props;

    return (
      <ContainerComponent title="Parish account settings">
        <form styleName="content" onSubmit={this.onSave}>
          <div styleName="field">
            <div styleName="field-title">Account name</div>
            <div styleName="input-wrapper">
              <InputControl type="big"
                            value={this.state.name}
                            readOnly={!isEditable}
                            onChange={this.onChangeName} />
            </div>
          </div>
          <div styleName="field">
            <div styleName="field-title">Account domain URL</div>
            <div styleName="input-wrapper">
              <InputControl type="big"
                            value={this.state.domainURL}
                            readOnly={!isEditable}
                            onChange={this.onChangeDomain} />
            </div>
          </div>
          {
            isEditable &&
              <div styleName="buttons-wrapper">
                <ButtonControl color="green"
                               type="submit"
                               disabled={!this.state.dirty || this.state.error}
                               value="Save changes"
                               onClick={this.onSave}/>
              </div>
          }
          {
            this.state.error &&
              <div styleName="field-error">
                {this.state.error}
              </div>
          }
        </form>
        <div styleName="deploy">
          {
            isEditable &&
              <div styleName="buttons-wrapper">
                <ButtonControl color="green"
                               disabled={this.state.dirty || this.state.error || this.state.isDeploying}
                               value="Deploy"
                               onClick={this.onDeploy}/>
              </div>
          }
          {
            !this.state.isDeploying && this.state.deployResult &&
              <div styleName="field-deploy">
                {this.state.deployResult == DEPLOY_RESULT_OK ? <div styleName="field-deploySuccess">{MSG_DEPLOY_COMPLETE}</div> : <div styleName="field-deployError">{MSG_DEPLOY_ERROR}  </div>}
              </div>
          }
          {
            this.state.isDeploying &&
              <LoaderAnimation></LoaderAnimation>
          }
        </div>
        <div styleName="wizard">
          {
            isEditable &&
              <div styleName="buttons-wrapper">
                <ButtonControl color="green"
                               disabled={this.state.isDeploying}
                               value="Open wizard"
                               onClick={this.onWizard}/>
              </div>
          }
        </div>
      </ContainerComponent>
    );
  }
}
