import React, {Component, PropTypes} from 'react';
import CSSModules from 'react-css-modules';
import InlineSVG from 'svg-inline-react';

import DropdownControl from 'components/elements/DropdownControl/DropdownControl';
import ContainerComponent from 'components/elements/ContainerComponent/ContainerComponent';
import InputControl from 'components/elements/InputControl/InputControl';
import {getModelByName} from 'utils/data';
import {ALERT_TYPE_CONFIRM} from 'components/modals/AlertModal/AlertModal';

import styles from './ContentList.sss';


@CSSModules(styles, {allowMultiple: true})
export default class ContentList extends Component {
  state = {
    items: [],
    itemTitle: "",

    activeModel: null,
    currentModel: null,

    availModels: []
  };
  activeInput = null;

  componentWillMount() {
    this.setState({
      items: this.props.items,
      currentModel: this.props.models[0]
    });
    this.checkAvailModels(this.props.items);
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.alertShowing && this.activeInput)
      this.activeInput.focus();
    this.setState({items: nextProps.items});
    if (nextProps.items != this.state.items)
      this.setState({itemTitle: ""});
    this.checkAvailModels(nextProps.items);
  }

  onChangeModel = name => {
    let model = getModelByName(name);
    this.setState({currentModel: model});
  };

  onItemTitleChange = event => {
    let title = event.target.value;
    this.setState({itemTitle: title});
  };

  onKeyDown = event => {
    if (this.props.alertShowing)
      return;
    //Enter pressed
    if (event.keyCode == 13) {
      this.onAddItem();
    //Esc pressed
    } else if (event.keyCode == 27) {
      this.setState({itemTitle: ""});
    }
  };

  onAddItem = event => {
    if (event)
      event.preventDefault();

    const {addItem} = this.props;
    addItem(this.state.itemTitle, this.state.currentModel);
    this.setState({itemTitle: ""});
  };

  onItemClick = item => {
    const {setCurrentItem} = this.props;
    setCurrentItem(item);
  };

  onModelClick = model => {
    if (this.state.activeModel && this.state.activeModel == model)
      this.setState({activeModel: null});
    else
      this.setState({activeModel: model, currentModel: model});
  };

  onRemoveClick = (event, item) => {
    event.stopPropagation();
    const {showAlert, deleteItem} = this.props;
    let title = item.title ? item.title : 'content item';

    let params = {
      type: ALERT_TYPE_CONFIRM,
      title: `Deleting ${title}`,
      description: "Are you sure?",
      onConfirm: () => deleteItem(item)
    };
    showAlert(params);
  };

  checkAvailModels (items) {
    let availModels = this.props.models.filter(model => {
      if (!model.single)
        return true;
      for (let item of items) {
        if (item.model == model)
          return false;
      }
      return true;
    });
    this.setState({availModels});
  };

  render() {
    const {isEditable, models} = this.props;

    return (
      <ContainerComponent title="Content">
        <div styleName="content-wrapper">
          <div styleName="filters">
            <div styleName="filters-item">
              <div styleName="filters-title">
                Content Types
              </div>
              {
                models.sort((A,B)=>{
                  let a = A.name.toLowerCase(),
                     b = B.name.toLowerCase();
                 if (a < b) return -1;
                 if (a > b) return 1;
                 return 0;
                }).map(model => {
                  let key = model.origin && model.origin.id ? model.origin.id : Math.random();

                  let styleName = "filters-type";
                  if (this.state.activeModel && this.state.activeModel != model)
                    styleName += " filters-typeHidden";

                  return(
                    <div styleName={styleName} key={key} onClick={() => this.onModelClick(model)}>
                      {model.name}
                    </div>
                  );
                })
              }
            </div>
          </div>
          <div styleName="list-wrapper">
            <div styleName="list">
              {
                this.state.items.length > 0 &&
                  <div styleName="list-item">
                    <div styleName="colorLabel"></div>
                    <div styleName="type"></div>
                    <div styleName="updated">UPDATED</div>
                  </div>
              }
              {
                this.state.items
                .filter((item) => !this.state.activeModel || this.state.activeModel == item.model)
                .map(item => {
                  let updatedDate = item.origin.updatedAt;
                  if (!updatedDate)
                    updatedDate = new Date();
                  let updatedStr = updatedDate.toLocaleString("en-US",
                    {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric'});

                  let colorStyle = {background: item.model.color};
                  let key = item.origin && item.origin.id ? item.origin.id : Math.random();

                  return(
                    <div styleName="list-item"
                         key={key}
                         onClick={() => this.onItemClick(item)} >
                      <div styleName="colorLabel" style={colorStyle}></div>
                      <div styleName="type">
                        <div styleName="name">{item.title}</div>
                        <div styleName="description">{item.model.name}</div>
                      </div>
                      <div styleName="updated"><div styleName="updated-inner">{updatedStr}</div></div>
                      {
                        isEditable &&
                          <div styleName="hidden-controls">
                            <div styleName="hidden-remove" onClick={event => this.onRemoveClick(event, item)}>
                              <InlineSVG styleName="cross"
                                         src={require("./cross.svg")}/>
                            </div>
                          </div>
                      }
                    </div>
                  );
                })
              }
            </div>
            {
              isEditable &&
                <div styleName="inputs-wrapper">
                  <div styleName="dropdown-wrapper">
                    <DropdownControl suggestionsList={this.state.availModels.map(m => m.name)}
                                     suggest={this.onChangeModel}
                                     current={this.state.currentModel.name} />
                  </div>
                  <div styleName="input-wrapper">
                    <InputControl placeholder="Create a new Content Record"
                                  value={this.state.itemTitle}
                                  autoFocus={true}
                                  icon="plus"
                                  onIconClick={this.onAddItem}
                                  onChange={this.onItemTitleChange}
                                  onKeyDown={this.onKeyDown}
                                  inputRef={c => this.activeInput = c} />
                  </div>
                </div>
            }
          </div>
        </div>
      </ContainerComponent>
    );
  }
}
