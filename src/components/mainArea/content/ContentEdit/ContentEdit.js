import React, {Component, PropTypes} from 'react';
import {Parse} from 'parse';
import CSSModules from 'react-css-modules';
import InlineSVG from 'svg-inline-react';
import _ from 'lodash/core';
import ReactStars from 'react-stars';

import 'flatpickr/dist/flatpickr.min.css';
import Flatpickr from 'react-flatpickr';

import 'medium-editor/dist/css/medium-editor.css';
import 'medium-editor/dist/css/themes/default.css';
import Editor from 'react-medium-editor';


import InputControl from 'components/elements/InputControl/InputControl';
import ButtonControl from 'components/elements/ButtonControl/ButtonControl';
import EditableTitleControl from 'components/elements/EditableTitleControl/EditableTitleControl';
import SwitchControl from 'components/elements/SwitchControl/SwitchControl';
import ContainerComponent from 'components/elements/ContainerComponent/ContainerComponent';
import DropdownControl from 'components/elements/DropdownControl/DropdownControl';
import {filterSpecials, trimFileExt} from 'utils/common';
import {MODAL_TYPE_MEDIA, MODAL_TYPE_REFERENCE, MODAL_TYPE_WYSIWYG} from 'ducks/nav';
import {store} from 'index';
import {MEDIA_TYPE__IMAGE, MEDIA_TYPE__PDF} from 'models/MediaItemData';

import * as ftps from 'models/ModelData';


import styles from './ContentEdit.sss';


@CSSModules(styles, {allowMultiple: true})
export default class ContentEdit extends Component {
  state = {
    title: "",
    fields: new Map(),
    dirty: false
  };
  item = null;
  fieldsErrors = new Map();

  componentWillMount() {
    this.item = this.props.item;
    this.setState({
      title:  this.item.title,
      fields: new Map(this.item.fields)
    });
  }

  updateItemTitle = title => {
    this.setState({title});
    for (let [field, value] of this.state.fields) {
      if (field.isTitle)
        this.setState({fields: this.state.fields.set(field, title)});
    }
  };

  onClose = () => {
    this.props.onClose();
  };

  onDiscard = () => {
    this.setState({fields: new Map(this.item.fields), dirty: false});
  };

  onSave = () => {
    //if (this.validate()) {
      this.item.fields = this.state.fields;
      this.props.updateItem(this.item);
      this.setState({dirty: false});
    //}
  };

  validate() {
    let isValid = true;

    for (let [field, value] of this.state.fields) {
      let error = null;
      let pattern = null;

      switch (field.type) {
        case ftps.FIELD_TYPE_SHORT_TEXT:
          switch (field.appearance) {
            case ftps.FIELD_APPEARANCE__SHORT_TEXT__SINGLE:
              if (!value && field.isTitle)
                error = "Title must be!";
              break;

            case ftps.FIELD_APPEARANCE__SHORT_TEXT__EMAIL:
              if (!value)
                break;

              pattern = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
              if (!pattern.test(value))
                error = "You must type a valid email!";
              break;

            case ftps.FIELD_APPEARANCE__SHORT_TEXT__URL:
              if (!value)
                break;

              pattern = new RegExp('^(https?:\\/\\/)' + // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
                '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
                '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
              if (!pattern.test(value))
                error = "You must type a valid URL!";
              break;
          }
          break;

        case ftps.FIELD_TYPE_LONG_TEXT:
          switch (field.appearance) {
            case ftps.FIELD_APPEARANCE__LONG_TEXT__MULTI:
              break;

            case ftps.FIELD_APPEARANCE__LONG_TEXT__WYSIWIG:
              break;
          }
          break;

        case ftps.FIELD_TYPE_FLOAT:
          switch (field.appearance) {
            case ftps.FIELD_APPEARANCE__FLOAT__DECIMAL:
              break;
          }
          break;

        case ftps.FIELD_TYPE_INTEGER:
          switch (field.appearance) {
            case ftps.FIELD_APPEARANCE__INTEGER__DECIMAL:
              if (!value)
                value = 0;
              if (Math.floor(value) != parseFloat(value))
                error = "You must type an integer value!";
              break;

            case ftps.FIELD_APPEARANCE__INTEGER__RATING:
              break;
          }
          break;

        case ftps.FIELD_TYPE_BOOLEAN:
          switch (field.appearance) {
            case ftps.FIELD_APPEARANCE__BOOLEAN__RADIO:
              break;

            case ftps.FIELD_APPEARANCE__BOOLEAN__SWITCH:
              break;
          }
          break;

        case ftps.FIELD_TYPE_FILE:
          switch (field.appearance) {
            case ftps.FIELD_APPEARANCE__FILE__IMAGE:
            case ftps.FIELD_APPEARANCE__FILE__PDF:
              break;
          }
          break;

        case ftps.FIELD_TYPE_DATE:
          switch (field.appearance) {
            case ftps.FIELD_APPEARANCE__DATE__DATE_TIME:
            case ftps.FIELD_APPEARANCE__DATE__DATE:
            case ftps.FIELD_APPEARANCE__DATE__TIME:
              break;
          }
          break;

        case ftps.FIELD_TYPE_ENUMERATION:
          break;
      }

      this.fieldsErrors.set(field, error);
      if (error)
        isValid = false;
    }

    return isValid;
  };

  onChange_SHORT_TEXT(event, field) {
    let value = event.target.value;
    this.setFieldValue(field, value);
  }

  onChange_LONG_TEXT(event, field) {
    let value = event.target.value;
    this.setFieldValue(field, value);
  }

  onChange_LONG_TEXT_WYSIWYG(text, field) {
    this.setFieldValue(field, text);
  }

  onChange_FLOAT(event, field) {
    let str = event.target.value;
    let value = parseFloat(str);
    this.setFieldValue(field, value);
  }

  onChange_INTEGER(event, field) {
    let str = event.target.value;
    let value = !!str ? parseFloat(str) : '';
    this.setFieldValue(field, value);
  }

  onChange_INTEGER__RATING(value, field) {
    value *= 2;
    this.setFieldValue(field, value);
  }

  onChange_BOOLEAN(value, field) {
    this.setFieldValue(field, value);
  }

  onChange_DATE(_date, field) {
    let date = _date[0];
    let oldDate = this.state.fields.get(field);
    if (!oldDate)
      oldDate = new Date();
    date.setHours(oldDate.getHours());
    date.setMinutes(oldDate.getMinutes());
    this.setFieldValue(field, date);
  }

  onChange_TIME(_time, field) {
    let time = _time[0];
    let date = this.state.fields.get(field);
    if (!date)
      date = new Date();
    date.setHours(time.getHours());
    date.setMinutes(time.getMinutes());
    this.setFieldValue(field, date);
  }

  onMediaChoose(field) {
    let type = field.appearance == ftps.FIELD_APPEARANCE__FILE__PDF ?
      MEDIA_TYPE__PDF :
      MEDIA_TYPE__IMAGE;

    this.props.showModal(MODAL_TYPE_MEDIA, {
      type,
      callback: mItem => this.setFieldValue(field, mItem)
    });
  }

  onMediaNew(event, field) {
    let type = field.appearance == ftps.FIELD_APPEARANCE__FILE__PDF ?
      MEDIA_TYPE__PDF :
      MEDIA_TYPE__IMAGE;

    let file = event.target.files[0];
    let parseFile = new Parse.File(filterSpecials(file.name), file, file.type);
    parseFile.save().then(() => {
      const {addMediaItem} = this.props;
      addMediaItem(parseFile, trimFileExt(file.name), type);

      let mItems = store.getState().media.items;
      let mItem = mItems[mItems.length - 1];
      this.setFieldValue(field, mItem);
    });
  }

  onMediaNameChange(event, field) {
    let mItem = this.state.fields.get(field);
    let value = event.target.value;
    mItem.name = value;
    this.props.updateMediaItem(mItem);
    this.setFieldValue(field, mItem);
  }

  onReferenceChoose(field) {
    this.props.showModal(MODAL_TYPE_REFERENCE,
      {
        type: field.appearance,
        callback: item => this.setFieldValue(field, item)
      }
    );
  }

  onReferenceNew(field) {
    //TODO
  }

  onShowWysiwygModal(field) {
    this.props.showModal(
      MODAL_TYPE_WYSIWYG,
      {
        text: this.state.fields.get(field),
        callback: text => this.setFieldValue(field, text)
      }
    );
  }

  setFieldValue(field, value) {
    let fields = this.state.fields;
    this.setState({fields: fields.set(field, value), dirty: true});
  }

  generateElement(field, value) {
    const {isEditable} = this.props;

    let title = (
      <div styleName="field-title">
        {field.name}
      </div>
    );

    let inner;

    switch (field.type) {
      case ftps.FIELD_TYPE_SHORT_TEXT:
        if (value === null) {
          value = "";
          this.setFieldValue(field, value);
        }

        switch (field.appearance) {
          case ftps.FIELD_APPEARANCE__SHORT_TEXT__SINGLE:
          case ftps.FIELD_APPEARANCE__SHORT_TEXT__EMAIL:

            inner = (
              <div styleName="input-wrapper">
                <InputControl ref={field.nameId}
                              type="big"
                              value={value}
                              readOnly={!isEditable}
                              onChange={e => this.onChange_SHORT_TEXT(e, field)} />
              </div>
            );
            break;

          case ftps.FIELD_APPEARANCE__SHORT_TEXT__URL:
            inner = (
              <div styleName="input-wrapper url">
                <InputControl type="big"
                              ref={field.nameId}
                              value={value}
                              readOnly={!isEditable}
                              onChange={e => this.onChange_SHORT_TEXT(e, field)} />
              </div>
            );
            break;
        }
        break;

      case ftps.FIELD_TYPE_LONG_TEXT:
        if (value === null) {
          value = "";
          this.setFieldValue(field, value);
        }

        switch (field.appearance) {
          case ftps.FIELD_APPEARANCE__LONG_TEXT__MULTI:
            inner = (
              <textarea styleName="textarea"
                        ref={field.nameId}
                        value={value}
                        readOnly={!isEditable}
                        onChange={e => this.onChange_LONG_TEXT(e, field)} />
            );
            break;

          case ftps.FIELD_APPEARANCE__LONG_TEXT__WYSIWIG:
            if (!value) {
              value = "<p></p>";
              this.setFieldValue(field, value);
            }

            title = (
              <div styleName="field-title">
                {field.name}
                <div styleName="link"
                     onClick={() => this.onShowWysiwygModal(field)} >
                  <InlineSVG styleName="link-icon" src={require('./link.svg')}/>
                </div>
              </div>
            );
            inner = (
              <Editor
                styleName="wysiwig"
                text={value}
                onChange={text => this.onChange_LONG_TEXT_WYSIWYG(text, field)}
                options={{
                  placeholder: false,
                  toolbar: {
                    buttons: [
                      'bold',
                      'italic',
                      'underline',
                      'anchor',
                      'quote',
                      'h2',
                      'h3',
                      'unorderedlist'
                    ]
                  } 
                }}
              />
            );
            break;
        }
        break;

      case ftps.FIELD_TYPE_FLOAT:
        if (value === null) {
          value = 0;
          this.setFieldValue(field, value);
        }

        switch (field.appearance) {
          case ftps.FIELD_APPEARANCE__FLOAT__DECIMAL:
            inner = (
              <div styleName="input-wrapper">
                <InputControl type="big"
                              ref={field.nameId}
                              value={value}
                              readOnly={!isEditable}
                              onChange={e => this.onChange_FLOAT(e, field)} />
              </div>
            );
            break;
        }
        break;

      case ftps.FIELD_TYPE_INTEGER:
        if (value === null) {
          value = 0;
          this.setFieldValue(field, value);
        }

        switch (field.appearance) {
          case ftps.FIELD_APPEARANCE__INTEGER__DECIMAL:
            inner = (
              <div styleName="input-wrapper">
                <InputControl type="big"
                              ref={field.nameId}
                              value={value}
                              readOnly={!isEditable}
                              onChange={e => this.onChange_INTEGER(e, field)}/>
              </div>
            );
            break;

          case ftps.FIELD_APPEARANCE__INTEGER__RATING:
            value *= .5;
            inner = (
              <div styleName="input-wrapper">
                <ReactStars
                  styleName="react-stars"
                  value={value}
                  onChange={e => this.onChange_INTEGER__RATING(e, field)}
                  size={32}
                  color1={'#F5F5F5'}
                  color2={'#5CA6DC'} />
              </div>
            );
            break;
        }
        break;

      case ftps.FIELD_TYPE_BOOLEAN:
        if (value === null)
          value = false;

        switch (field.appearance) {
          case ftps.FIELD_APPEARANCE__BOOLEAN__RADIO:
            let id1 = _.uniqueId('radio1_');
            let id2 = _.uniqueId('radio2_');
            inner = (
              <div styleName="radio-wrapper">
                <div styleName="radio-button">
                  <input styleName="radio"
                         type="radio"
                         id={id1}
                         name="radio"
                         checked={value}
                         onChange={e => this.onChange_BOOLEAN(true, field)} />
                  <label styleName="radio-label" htmlFor={id1}>Yes</label>
                </div>
                <div styleName="radio-button">
                  <input styleName="radio"
                         type="radio"
                         id={id2}
                         name="radio"
                         checked={!value}
                         onChange={e => this.onChange_BOOLEAN(false, field)} />
                  <label styleName="radio-label" htmlFor={id2}>No</label>
                </div>
              </div>
            );
            break;

          case ftps.FIELD_APPEARANCE__BOOLEAN__SWITCH:
            let onChange = v => this.onChange_BOOLEAN(v, field);
            inner = (
              <div styleName="switch-wrapper">
                <SwitchControl checked={value} onChange={isEditable ? onChange : null}/>
              </div>
            );
            break;
        }
        break;

      case ftps.FIELD_TYPE_FILE:
        switch (field.appearance) {
          case ftps.FIELD_APPEARANCE__FILE__IMAGE:

            if (value && value.file) {
              let imgStyle = {backgroundImage: `url(${value.file.url()})`};
              inner = (
                <div styleName="media">
                  <div styleName="media-item">
                    <div styleName="media-header">
                      <input type="text"
                             placeholder="Image name"
                             onChange={e => this.onMediaNameChange(e, field)}
                             value={value.name} />
                      <InlineSVG styleName="media-cross"
                                 src={require('./cross.svg')}
                                 onClick={() => this.setFieldValue(field, null)} />
                    </div>
                    <div styleName="media-content" style={imgStyle}>
                    </div>
                  </div>
                </div>
              );

            } else {
              inner = (
                <div styleName="media">
                  <div styleName="media-buttons">
                    <div type="file" styleName="media-button media-upload">
                      Upload New
                      <input styleName="media-hidden"
                             type="file"
                             onChange={e => this.onMediaNew(e, field)} />
                    </div>
                    <div styleName="media-button media-insert"
                         onClick={() => this.onMediaChoose(field)}>
                      Insert Existing
                    </div>
                  </div>
                </div>
              );
            }
            break;

          case ftps.FIELD_APPEARANCE__FILE__PDF:

            if (value && value.file) {
              inner = (
                <div styleName="media">
                  <div styleName="media-item">
                    <div styleName="media-header">
                      <input type="text"
                             placeholder="PDF name"
                             onChange={e => this.onMediaNameChange(e, field)}
                             value={value.name} />
                      <InlineSVG styleName="media-cross"
                                 src={require('./cross.svg')}
                                 onClick={() => this.setFieldValue(field, null)} />
                    </div>
                  </div>
                </div>
              );

            } else {
              inner = (
                <div styleName="media">
                  <div styleName="media-buttons">
                    <div type="file" styleName="media-button media-upload">
                      Upload New
                      <input styleName="media-hidden"
                             type="file"
                             onChange={e => this.onMediaNew(e, field)} />
                    </div>
                    <div styleName="media-button media-insert"
                         onClick={() => this.onMediaChoose(field)}>
                      Insert Existing
                    </div>
                  </div>
                </div>
              );
            }

            break;
        }
        break;

      case ftps.FIELD_TYPE_DATE:
        switch (field.appearance) {
          case ftps.FIELD_APPEARANCE__DATE__DATE_TIME:
            if (value === null) {
              value = new Date();
              this.setFieldValue(field, value);
            }

            inner = (
              <div styleName="input-wrapper data-time-wrapper">
                <div styleName="date">
                  <Flatpickr value={value}
                             data-click-opens={isEditable}
                             data-alt-input="true"
                             onChange={obj => this.onChange_DATE(obj, field)} />
                </div>
                <div styleName="time">
                  <Flatpickr value={value}
                             data-click-opens={isEditable}
                             data-no-calendar={true}
                             data-enable-time={true}
                             data-alt-format="h:i K"
                             data-alt-input="true"
                             onChange={obj => this.onChange_TIME(obj, field)} />
                </div>
              </div>
            );
            break;

          case ftps.FIELD_APPEARANCE__DATE__DATE:
            if (value === null) {
              value = new Date();
              this.setFieldValue(field, value);
            }

            inner = (
              <div styleName="input-wrapper data-time-wrapper">
                <div styleName="date">
                  <Flatpickr value={value}
                             data-click-opens={isEditable}
                             data-alt-input="true"
                             onChange={obj => this.onChange_DATE(obj, field)} />
                </div>
              </div>
            );
            break;

          case ftps.FIELD_APPEARANCE__DATE__TIME:
            if (value === null) {
              value = new Date();
              this.setFieldValue(field, value);
            }

            inner = (
              <div styleName="input-wrapper data-time-wrapper">
                <div styleName="time">
                  <Flatpickr value={value}
                             data-click-opens={isEditable}
                             data-no-calendar={true}
                             data-enable-time={true}
                             data-alt-format="h:i K"
                             data-alt-input="true"
                             onChange={obj => this.onChange_TIME(obj, field)} />
                </div>
              </div>
            );
            break;
        }
        break;

      case ftps.FIELD_TYPE_REFERENCE:
        if (value) {
          inner = (
            <div styleName="reference">
              <div styleName="reference-item">
                <input type="text" value={value.title} readOnly />
                <InlineSVG styleName="reference-cross"
                           src={require('./cross.svg')}
                           onClick={() => this.setFieldValue(field, null)} />
              </div>
            </div>
          );
        } else {
          inner = (
            <div styleName="reference">
              <div styleName="reference-buttons">
                <div styleName="reference-button reference-insert" onClick={() => this.onReferenceChoose(field)}>
                  Insert Existing Entry
                </div>
              </div>
            </div>
          );
        }
        break;

      case ftps.FIELD_TYPE_ENUMERATION:
        let values = field.appearance;
        if (!values.length)
          break;

        if (value === null) {
          value = values[0];
          this.setFieldValue(field, value);
        }

        inner = (
          <div styleName="input-wrapper">
            <DropdownControl suggestionsList={values}
                             suggest={v => this.setFieldValue(field, v)}
                             current={value} />
          </div>
        );
        break;

      default:
        break;
    }

    let error = this.fieldsErrors.get(field);

    return (
      <div styleName="field" key={field.nameId}>
        {title}
        {inner}
        {
          error &&
            <div styleName="field-error">
              {error}
            </div>
        }
      </div>
    );
  }

  generateContent() {
    const {isEditable} = this.props;

    let content = [];
    for (let [field, value] of this.state.fields) {
      let elm = this.generateElement(field, value);
      content.push(elm);
    }

    return (
      <div styleName="content">
        {content}
        {
          isEditable &&
            <div styleName="buttons-wrapper">
              <div styleName="button-save">
                <ButtonControl color="green"
                               value="Save"
                               disabled={!this.state.dirty}
                               onClick={this.onSave}/>
              </div>
              <ButtonControl color="gray"
                             value="Discard changes"
                             disabled={!this.state.dirty}
                             onClick={this.onDiscard}/>
            </div>
        }
      </div>
    );
  }

  render() {
    const {isEditable} = this.props;

    let content = this.generateContent();

    let titles = (
      <div>
        <EditableTitleControl text={this.state.title}
                              placeholder={"Item title"}
                              update={isEditable ? this.updateItemTitle : null}
                              cancel={this.endEdit} />
        <EditableTitleControl text={this.item.model.name}
                              isSmall={true} />
      </div>
    );

    return (
      <ContainerComponent hasTitle2={true}
                          titles={titles}
                          onClickBack={this.onClose}>
        {content}
      </ContainerComponent>
    );
  }
}
