import {Parse} from 'parse';

import {store} from 'index';
import {getLocalStorage} from 'ducks/user';
import {setModelsStructure} from 'ducks/models';


export const INITIALIZE_APP_END     = 'app/initialize/INITIALIZE_APP_END';


const SERVER = process.env.NODE_ENV === 'production' ?
    "http://parish.host/parse" : "http://localhost:1337/parse";

const APP_ID = "parish-connect";


function subInitParse() {
  Parse.initialize(APP_ID);
  Parse.serverURL = SERVER;
}

export function initApp() {
  return dispatch => {
    subInitParse();
  
    Parse.Cloud.run('initModels')
      .then(res => {
        store.dispatch(setModelsStructure(res.modelsStructure));
        store.dispatch(getLocalStorage());
        dispatch({
          type: INITIALIZE_APP_END
        });
      }, e => {
        setTimeout(() => {
          localStorage.clear();
          window.location = "/";
        }, 1000);
      });
  }
}


const initialState = {
  initializedApp: false
};

export default function initializeReducer(state = initialState, action) {
  switch (action.type) {
    case INITIALIZE_APP_END:
      return {...state, initializedApp: true};

    default:
      return state;
  }
}
