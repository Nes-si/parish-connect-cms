import {Parse} from 'parse';

import {store} from '../index';
import {ContentItemData} from 'models/ContentData';
import {FIELD_ADD, FIELD_UPDATED, FIELD_DELETED} from 'ducks/models';
import {LOGOUT} from './user';
import {getRandomColor} from 'utils/common';
import {getContentForModel} from 'utils/data';


export const INIT_END           = 'app/content/INIT_END';
export const ITEM_ADD           = 'app/content/ITEM_ADD';
export const ITEM_UPDATED       = 'app/content/ITEM_UPDATED';
export const ITEM_DELETED       = 'app/content/ITEM_DELETED';
export const SET_CURRENT_ITEM   = 'app/content/SET_CURRENT_ITEM';

export const DEPLOY_START       = 'app/content/DEPLOY_START';
export const DEPLOY_FINISH      = 'app/content/DEPLOY_FINISH';

export const DEPLOY_RESULT_OK   = "DEPLOY_RESULT_OK";
export const DEPLOY_RESULT_ERROR= "DEPLOY_RESULT_ERROR";


function requestContentItems(model, items) {
  return new Promise((resolve, reject) => {
    new Parse.Query(Parse.Object.extend(model.nameId))
      .find()
      .then(items_o => {
        for (let item_o of items_o) {
          let item = new ContentItemData();
          item.model = model;
          item.setOrigin(item_o);
          items.push(item);
        }
        resolve();
      }, reject);
  });
}

export function init() {
  return dispatch => {
    let items = [];
    let promises = [];
    for (let model of store.getState().models.models) {
      promises.push(requestContentItems(model, items));
    }

    Promise.all(promises)
      .then(() =>
        dispatch({
          type: INIT_END,
          items
        })
      )
      .catch(e => console.log(e));
  };
}

export function addItem(title, model) {
  let item = new ContentItemData();

  item.model = model;

  item.title = title;
  item.site = store.getState().models.currentSite;

  item.updateOrigin();
  item.origin.save();

  return {
    type: ITEM_ADD,
    item
  };
}

export function updateItem(item) {
  item.updateOrigin();
  item.origin.save();

  return {
    type: ITEM_UPDATED
  };
}

export function setCurrentItem(currentItem) {
  return {
    type: SET_CURRENT_ITEM,
    currentItem
  };
}

export function deleteItem(item) {
  let items = store.getState().content.items;
  items.splice(items.indexOf(item), 1);

  item.origin.destroy();

  return {
    type: ITEM_DELETED,
    item
  };
}

export function deploy(site) {
  return dispatch => {
    dispatch({type: DEPLOY_START});
    Parse.Cloud.run('deploy', {domain: site.domainURL.replace('http://',''), pAccountId: site.origin.id})
      .then(
        () => dispatch({
          type: DEPLOY_FINISH,
          deployResult: DEPLOY_RESULT_OK
        }),
        e => {
          console.log(e);
          dispatch({
            type: DEPLOY_FINISH,
            deployResult: DEPLOY_RESULT_ERROR
          });
        }
      );
  }
}

const initialState = {
  items: [],
  currentItem: null,

  isDeploying: false,
  deployResult: null
};

export default function contentReducer(state = initialState, action) {
  switch (action.type) {
    case INIT_END:
      for (let item of action.items)
        item.postInit(action.items);
      return {
        ...state,
        items: action.items
      };

    case ITEM_ADD:
      let items = state.items;
      items.push(action.item);
      return {
        ...state,
        items
      };

    case ITEM_UPDATED:
    case ITEM_DELETED:
      return {...state};

    case SET_CURRENT_ITEM:
      return {
        ...state,
        currentItem: action.currentItem,
      };

    case FIELD_ADD:
    case FIELD_UPDATED:
    case FIELD_DELETED:
      let model = action.field.model;
      let modelItems = getContentForModel(model);
      for (let item of modelItems) {
        item.model = model;
      }
      return {...state};

    case LOGOUT:
      return {
        ...state,
        currentItem: null
      };

    case DEPLOY_START:
      return {
        ...state,
        isDeploying: true
      };

    case DEPLOY_FINISH:
      return {
        ...state,
        isDeploying: false,
        deployResult: action.deployResult
      };

    default:
      return state;
  }
}
