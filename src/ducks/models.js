import {Parse} from 'parse';

import {store} from '../index';
import {UserData, CollaborationData, ROLE_ADMIN} from 'models/UserData';
import {SiteData} from 'models/ModelData';
import {LOGOUT} from './user';

export const SET_MODELS_STRUCTURE = 'app/models/SET_MODELS_STRUCTURE';
export const INIT_END             = 'app/models/INIT_END';

export const SITE_ADD             = 'app/models/SITE_ADD';
export const SITE_UPDATED         = 'app/models/SITE_UPDATED';
export const COLLABORATION_ADD    = 'app/models/COLLABORATION_ADD';
export const COLLABORATION_UPDATE = 'app/models/COLLABORATION_UPDATE';
export const COLLABORATION_DELETE = 'app/models/COLLABORATION_DELETE';

export const SET_CURRENT_SITE     = 'app/models/SET_CURRENT_SITE';


export function setModelsStructure(modelsStructure) {
  return {
    type: SET_MODELS_STRUCTURE,
    modelsStructure
  };
}

function requestCollaborationsPre() {
  return new Promise((resolve, reject) => {
    new Parse.Query(CollaborationData.OriginClass)
      .equalTo("user", Parse.User.current())
      .find()
      .then(collabs => {
        let sites = [];
        for (let collab of collabs) {
          sites.push(collab.get('parishAccount'));
        }

        Parse.Object.fetchAllIfNeeded(sites)
          .then(() => resolve(sites), reject);
      }, reject);
  });
}

function requestUserSites() {
  return new Promise((resolve, reject) => {
    new Parse.Query(SiteData.OriginClass)
      .equalTo("owner", Parse.User.current())
      .find()
      .then(resolve, reject);
  });
}

function requestCollaborationsPost(sites_o, sites) {
  return new Promise((resolve, reject) => {
    new Parse.Query(CollaborationData.OriginClass)
      .containedIn("parishAccount", sites_o)
      .find()
      .then(collabs_o => {
        let promises = [];
        for (let collab_o of collabs_o) {
          let collab = new CollaborationData().setOrigin(collab_o);
          
          promises.push(new Promise((inResolve, inReject) => {
            collab_o.get('user')
              .fetch()
              .then(user_o => {
                let user = new UserData().setOrigin(user_o);
                collab.user = user;
                inResolve();
              }, inReject);
          }));
          
          let site_o = collab_o.get("parishAccount");
          for (let site of sites) {
            if (site.origin.id == site_o.id) {
              collab.pAccount = site;
              site.collaborations.push(collab);
              break;
            }
          }
        }
        
        Promise.all(promises)
          .then(() => resolve())
          .catch(reject);
      }, reject);
  });
}

export function init() {
  return dispatch => {
    let sites_o = [];
    let sites = [];
    
    requestCollaborationsPre()
      .then(sitesCollab_o => {
        sites_o = sitesCollab_o;
        return requestUserSites();
      })
      .then(sitesUser_o => {
        sites_o = sites_o.concat(sitesUser_o);
        let promises = [];
        for (let site_o of sites_o) {
          let site = new SiteData().setOrigin(site_o);
  
          promises.push(new Promise((inResolve, inReject) => {
            site_o.get('owner')
              .fetch()
              .then(owner_o => {
                let owner = new UserData().setOrigin(owner_o);
                site.owner = owner;
                inResolve();
              }, inReject);
          }));
          
          sites.push(site);
        }
  
        return Promise.all(promises)
          .then(() => requestCollaborationsPost(sitesUser_o, sites));
      })
      .then(() =>
        dispatch({
          type: INIT_END,
          sites
        })
      )
      .catch(e => console.log(e));
  }
}

export function setCurrentSite(currentSite) {
  let userData = store.getState().user.userData;
  let isOwner = userData.origin.id == currentSite.owner.origin.id;

  let checkIsAdmin = () => {
    for (let collab of currentSite.collaborations) {
      if (collab.user.origin.id == userData.origin.id && collab.role == ROLE_ADMIN)
        return true;
    }
    return false;
  };
  let isAdmin = checkIsAdmin();

  return {
    type: SET_CURRENT_SITE,
    currentSite,
    isOwner,
    isAdmin
  };
}

export function addSite(site) {
  let sites = store.getState().models.sites;
  sites.push(site);
  
  site.owner = store.getState().user.userData;
  site.updateOrigin();
  site.origin.save()
    .then(acc => setTimeout(() => window.location.href = "/wizard.html?id=" + acc.id, 200));
  
  return {
    type: SITE_ADD,
    site,
    sites
  };
}

export function updateSite(site) {
  site.updateOrigin();
  site.origin.save();
  
  return {
    type: SITE_UPDATED
  };
}

export function addCollaboration(user) {
  let collab = new CollaborationData();
  collab.user = user;
  
  let currentSite = store.getState().models.currentSite;
  collab.pAccount = currentSite;
  
  currentSite.collaborations.push(collab);
  collab.updateOrigin();
  collab.origin.save();
  
  return {
    type: COLLABORATION_ADD
  };
}

export function updateCollaboration(collab) {
  collab.updateOrigin();
  collab.origin.save();
  
  return {
    type: COLLABORATION_UPDATE
  };
}

export function deleteCollaboration(collab) {
  let collabs = store.getState().models.currentSite.collaborations;
  collabs.splice(collabs.indexOf(collab), 1);
  
  collab.origin.destroy();
  
  return {
    type: COLLABORATION_DELETE
  };
}

const initialState = {
  sites: [],
  currentSite: null,

  isOwner: false,
  isAdmin: false,
  
  models: []
};

export default function modelsReducer(state = initialState, action) {
  switch (action.type) {
    case SET_MODELS_STRUCTURE:
      return {
        ...state,
        models: action.modelsStructure
      };
    
    case INIT_END:
    case SITE_ADD:
      return {
        ...state,
        sites: action.sites
      };
  
    case SET_CURRENT_SITE:
      return {
        ...state,
        currentSite:  action.currentSite,
        isOwner: action.isOwner,
        isAdmin: action.isAdmin
      };

    case SITE_UPDATED:
    case COLLABORATION_ADD:
    case COLLABORATION_UPDATE:
    case COLLABORATION_DELETE:
      return {...state};
    
    case LOGOUT:
      return {
        ...state,
        currentModel: null,
        currentSite: null,
        isOwner: false,
        isAdmin: false
      };
    
    default:
      return state;
  }
}