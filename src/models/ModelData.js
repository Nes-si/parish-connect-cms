import {Parse} from 'parse';

import {removeRedundantSpaces, filterSpecials} from 'utils/common';


export class SiteData {
  static get OriginClass() {return Parse.Object.extend("ParishAccount");}
  
  origin = null;
  
  domainURL = "";
  colorMain = '#000000';
  
  //setter
  _name = "";
  
  //links
  owner = null;
  
  //children
  collaborations = [];
  
  get name() {return this._name;}
  set name(name) {
    this._name = removeRedundantSpaces(name);
    this.nameId = this._name;
  }
  
  setOrigin(origin) {
    this.origin = origin;
    
    if (origin.get('name'))       this._name      = origin.get('name');
    if (origin.get('domainURL'))  this.domainURL  = origin.get('domainURL');
    if (origin.get('colorMain'))  this.colorMain  = origin.get('colorMain');
    
    return this;
  }
  
  updateOrigin() {
    if (!this.origin)
      this.origin = new SiteData.OriginClass;
    
    this.origin.set("name",       this.name);
    this.origin.set("domainURL",  this.domainURL);
    this.origin.set("colorMain",  this.colorMain);
    
    this.origin.set("owner",      this.owner.origin);
  }
}




export const FIELD_TYPE_SHORT_TEXT  = "Short Text";
export const FIELD_TYPE_LONG_TEXT   = "Long Text";
export const FIELD_TYPE_INTEGER     = "Number Int";
export const FIELD_TYPE_FLOAT       = "Number";
export const FIELD_TYPE_BOOLEAN     = "Boolean";
export const FIELD_TYPE_DATE        = "Date/time";
export const FIELD_TYPE_FILE        = "File";
export const FIELD_TYPE_REFERENCE   = "Reference";
export const FIELD_TYPE_COLOR       = "Color";
export const FIELD_TYPE_ENUMERATION = "Enumeration";


export const FIELD_APPEARANCE__SHORT_TEXT__SINGLE   = "Single line";
export const FIELD_APPEARANCE__SHORT_TEXT__URL      = "URL";
export const FIELD_APPEARANCE__SHORT_TEXT__EMAIL    = "Email";

export const FIELD_APPEARANCE__LONG_TEXT__MULTI     = "Multi-line";
export const FIELD_APPEARANCE__LONG_TEXT__WYSIWIG   = "WYSIWYG";

export const FIELD_APPEARANCE__INTEGER__DECIMAL     = "Decimal";
export const FIELD_APPEARANCE__INTEGER__RATING      = "Rating";

export const FIELD_APPEARANCE__FLOAT__DECIMAL       = "Decimal";

export const FIELD_APPEARANCE__BOOLEAN__RADIO       = "Radio buttons";
export const FIELD_APPEARANCE__BOOLEAN__SWITCH      = "Switch";

export const FIELD_APPEARANCE__DATE__DATE_TIME      = "Date & time";
export const FIELD_APPEARANCE__DATE__DATE           = "Date";
export const FIELD_APPEARANCE__DATE__TIME           = "Time";

export const FIELD_APPEARANCE__FILE__IMAGE          = "Image";
export const FIELD_APPEARANCE__FILE__PDF            = "PDF";

export const FIELD_APPEARANCE__COLOR__COLOR         = "Color";


export const FIELD_TYPES = new Map([
  [FIELD_TYPE_SHORT_TEXT, [
    FIELD_APPEARANCE__SHORT_TEXT__SINGLE,
    FIELD_APPEARANCE__SHORT_TEXT__URL,
    FIELD_APPEARANCE__SHORT_TEXT__EMAIL
  ]],
  [FIELD_TYPE_LONG_TEXT, [
    FIELD_APPEARANCE__LONG_TEXT__MULTI,
    FIELD_APPEARANCE__LONG_TEXT__WYSIWIG
  ]],
  [FIELD_TYPE_INTEGER, [
    FIELD_APPEARANCE__INTEGER__DECIMAL,
    FIELD_APPEARANCE__INTEGER__RATING
  ]],
  [FIELD_TYPE_FLOAT, [
    FIELD_APPEARANCE__FLOAT__DECIMAL
  ]],
  [FIELD_TYPE_BOOLEAN, [
    FIELD_APPEARANCE__BOOLEAN__RADIO,
    FIELD_APPEARANCE__BOOLEAN__SWITCH
  ]],
  [FIELD_TYPE_DATE, [
    FIELD_APPEARANCE__DATE__DATE_TIME,
    FIELD_APPEARANCE__DATE__DATE,
    FIELD_APPEARANCE__DATE__TIME
  ]],
  [FIELD_TYPE_FILE, [
    FIELD_APPEARANCE__FILE__IMAGE,
    FIELD_APPEARANCE__FILE__PDF
  ]],
  [FIELD_TYPE_COLOR, [
    FIELD_APPEARANCE__COLOR__COLOR
  ]],
  [FIELD_TYPE_REFERENCE, [

  ]],
  [FIELD_TYPE_ENUMERATION, [

  ]],
]);