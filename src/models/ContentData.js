import {Parse} from 'parse';

import {removeRedundantSpaces} from 'utils/common';
import {getMediaByO, getContentByO, getSiteByO} from 'utils/data';
import {FIELD_TYPE_FILE, FIELD_TYPE_REFERENCE} from 'models/ModelData';


export class ContentItemData {
  origin = null;
  
  site = null;
  fields = new Map();
  
  //setter
  _titleField = null;
  
  //links
  _model = null;
  
  get titleField() {return this._titleField;}
  
  get title() {return this.fields.get(this._titleField);}
  set title(title) {
    if (!this.titleField)
      return;
    
    title = removeRedundantSpaces(title);
    this.fields.set(this.titleField, title);
  }
  
  get model() {return this._model;}
  set model(model) {
    this._model = model;
    
    let title = this.title;
    this.fields = new Map();
    for (let field of this.model.fields) {
      this.fields.set(field, null);
      
      if (field.isTitle)
        this._titleField = field;
    }
    if (title)
      this.title = title;
  }
  
  get OriginClass() {
    if (this.model)
      return Parse.Object.extend(this.model.nameId);
    return null;
  }
  
  setOrigin(origin) {
    this.origin = origin;
    if (!this.model.common)
      this.site = getSiteByO(origin.get('account'));
    
    for (let field of this.model.fields) {
      let value = origin.get(field.nameId);
      if (field.type == FIELD_TYPE_FILE)
        this.fields.set(field, getMediaByO(value));
      else
        this.fields.set(field, value);
  
      if (field.isTitle)
        this._titleField = field;
    }
    
    return this;
  }
  
  postInit(items) {
    for (let field of this.model.fields) {
      if (field.type == FIELD_TYPE_REFERENCE)
        this.fields.set(field, getContentByO(this.origin.get(field.nameId), items));
    }
  }
  
  updateOrigin() {
    if (!this.origin)
      this.origin = new this.OriginClass;
  
    if (!this.model.common)
      this.origin.set('account', this.site.origin);
  
    for (let [field, value] of this.fields) {
      if ((field.type == FIELD_TYPE_FILE || field.type == FIELD_TYPE_REFERENCE) && value)
        this.origin.set(field.nameId, value.origin);
      else
        this.origin.set(field.nameId, value);
    }
  }
}