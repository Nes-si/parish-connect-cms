import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import CSSModules from 'react-css-modules';

import ButtonControl from 'components/elements/ButtonControl/ButtonControl';

import {login, register, ERROR_WRONG_PASS, ERROR_USER_EXISTS, ERROR_WRONG_CODE} from 'ducks/user';

import styles from './Sign.sss';


@CSSModules(styles, {allowMultiple: true})
export class Sign extends Component  {
  state = {
    isReg: false,
    email: '',
    password: '',
    code: '',
    emptyFields: false,
    authError: null
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      authError: nextProps.authError
    });
  }

  onEmailChange = event => {
    this.setState({
      email: event.target.value,
      emptyFields: false,
      authError: null
    });
  };

  onPasswordChange = event => {
    this.setState({
      password: event.target.value,
      emptyFields: false,
      authError: null
    });
  };

  onCodeChange = event => {
    this.setState({
      code: event.target.value,
      emptyFields: false,
      authError: null
    });
  };

  onSubmit = event => {
    event.preventDefault();
    const {login, register} = this.props.userActions;

    if (this.state.email.length && this.state.password.length) {
      if (this.state.isReg) {
        if (this.state.code.length)
          register(this.state.email, this.state.password, this.state.code);
        else
          this.setState({emptyFields: true});
      } else {
        login(this.state.email, this.state.password);
      }
    } else {
      this.setState({emptyFields: true});
    }
  };

  onLogIn = () => {
    this.setState({isReg: false});
  };
  onRegister = () => {
    this.setState({isReg: true});
  };

  render() {
    let contents = (
      <div styleName="Sign">
        <div styleName="title">Welcome to Parse connect</div>

        <form styleName="form" onSubmit={this.onSubmit}>
          <input styleName="input"
                 type="text"
                 placeholder="Enter email"
                 onChange={this.onEmailChange} />
          <input styleName="input"
                 type="password"
                 placeholder="Enter password"
                 onChange={this.onPasswordChange} />
          {
            this.state.isReg &&
              <input styleName="input"
                     type="password"
                     placeholder="Enter code"
                     onChange={this.onCodeChange}/>
          }
          <div styleName="button">
            <ButtonControl color="green"
                           type="submit"
                           value={this.state.isReg ? "Register" : "Log in"}
                           onClick={this.onSubmit} />
          </div>
        </form>

        <div styleName="errors">
          {
            this.state.authError ==  ERROR_WRONG_PASS &&
              <div styleName="error">Wrong email or password!</div>
          }
          {
            this.state.authError ==  ERROR_WRONG_CODE &&
              <div styleName="error">Wrong code!</div>
          }
          {
            this.state.authError ==  ERROR_USER_EXISTS &&
              <div styleName="error">This user already exists!</div>
          }
          {
            this.state.emptyFields &&
              <div styleName="error">You must type all fields</div>
          }
        </div>

        {
          this.state.isReg ?
            <div styleName="tab" onClick={this.onLogIn}>Click to log in</div> :
            <div styleName="tab" onClick={this.onRegister}>Click to register</div>
        }
      </div>
    );

    return (
      <div>
        {contents}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    authError: state.user.authError
  };
}

function mapDispatchToProps(dispatch) {
  return {
    userActions:  bindActionCreators({login, register}, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Sign);
