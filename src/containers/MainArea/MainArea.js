import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import CSSModules from 'react-css-modules';

import ContentList from 'components/mainArea/content/ContentList/ContentList';
import ContentEdit from 'components/mainArea/content/ContentEdit/ContentEdit';
import Sharing from 'components/mainArea/sharing/Sharing';
import Settings from 'components/mainArea/settings/Settings';
import {updateSite, addCollaboration, updateCollaboration, deleteCollaboration} from 'ducks/models';
import {addItem, updateItem, setCurrentItem, deleteItem, deploy} from 'ducks/content';
import {addMediaItem, updateMediaItem, removeMediaItem} from 'ducks/media';
import {PAGE_CONTENT, PAGE_SETTINGS, PAGE_SHARING, showAlert, closeContentItem, showModal} from 'ducks/nav';
import InlineSVG from 'svg-inline-react';

import styles from './MainArea.sss';


@CSSModules(styles, {allowMultiple: true})
export class MainArea extends Component  {
  render() {
    const {models, content, nav} = this.props;
    const {updateSite, addCollaboration, updateCollaboration, deleteCollaboration} = this.props.modelsActions;
    const {addItem, updateItem, setCurrentItem, deploy} = this.props.contentActions;
    const {showAlert, closeContentItem, showModal} = this.props.navActions;
    const {addMediaItem, updateMediaItem, removeMediaItem} = this.props.mediaActions;

    let isEditable = models.isOwner || models.isAdmin;
    let curSite = models.currentSite;

    let Area = (
      <div styleName="start-working">
        <InlineSVG styleName="hammer" src={require("./hammer.svg")}/>
        Add new site to start working
        <div styleName="hint">Find "Add new site" button at sidebar</div>
      </div>
    );
    
    switch (nav.openedPage) {
      case PAGE_CONTENT:
        if (nav.openedContentItem) {
          Area = (
            <ContentEdit item={content.currentItem}
                         onClose={closeContentItem}
                         updateItem={updateItem}
                         addMediaItem={addMediaItem}
                         updateMediaItem={updateMediaItem}
                         removeMediaItem={removeMediaItem}
                         showModal={showModal}
                         isEditable={isEditable}/>
          );
        } else if (curSite) {
          let items = [];
          for (let item of content.items) {
            if (item.model.common || item.site == curSite)
              items.push(item);
          }
          Area = (
            <ContentList items={items}
                         models={models.models}
                         setCurrentItem={setCurrentItem}
                         addItem={addItem}
                         deleteItem={deleteItem}
                         showAlert={showAlert}
                         alertShowing={nav.alertShowing}
                         isEditable={isEditable}/>
          );
        }

        break;

      case PAGE_SHARING:
        if (curSite) {
          Area = (
            <Sharing collaborations={curSite.collaborations}
                     owner={curSite.owner}
                     addCollaboration={addCollaboration}
                     updateCollaboration={updateCollaboration}
                     deleteCollaboration={deleteCollaboration}
                     showAlert={showAlert}
                     alertShowing={nav.alertShowing}
                     isEditable={models.isOwner} />
          );
        }

        break;

      case PAGE_SETTINGS:
        if (curSite) {
          Area = (
            <Settings site={curSite}
                      updateSite={updateSite}
                      deploy={deploy}
                      isDeploying={content.isDeploying}
                      deployResult={content.deployResult}
                      isEditable={models.isOwner} />
          );
        } else {
          Area = (
            <div styleName="start-working">
              <InlineSVG styleName="hammer" src={require("./hammer.svg")}/>
              There is no any settings yet
            </div>
          );
        }
        break;
    }

    return (
      <div styleName="mainArea">
        {Area}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    models:   state.models,
    content:  state.content,
    nav:      state.nav
  };
}

function mapDispatchToProps(dispatch) {
  return {
    modelsActions:  bindActionCreators({updateSite, addCollaboration, updateCollaboration, deleteCollaboration}, dispatch),
    contentActions: bindActionCreators({addItem, updateItem, setCurrentItem, deleteItem, deploy}, dispatch),
    navActions:     bindActionCreators({showAlert, closeContentItem, showModal}, dispatch),
    mediaActions:   bindActionCreators({addMediaItem, updateMediaItem, removeMediaItem}, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MainArea);
