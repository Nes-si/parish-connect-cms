import {Parse} from 'parse';

import {store} from '../index';
import {UserData} from 'models/UserData';
import {removeRedundantSpaces, filterSpecials} from 'utils/common';


export function checkSiteName(name, curSite) {
  if (!name)
    return false;
  
  name = removeRedundantSpaces(name);
  
  let sites = store.getState().models.sites;
  for (let site of sites) {
    if (site != curSite && site.name == name)
      return false;
  }
  return true;
}
export function checkSiteURL(URL, curSite) {
  if (!URL)
    return false;
  
  let sites = store.getState().models.sites;
  for (let site of sites) {
    if (site != curSite && site.domainURL == URL)
      return false;
  }
  return true;
}


export function getUser(username) {
  if (!username)
    return Promise.reject();
  
  return new Promise((resolve, reject) => {
    new Parse.Query(Parse.User)
      .equalTo("username", username)
      .first()
      .then(user_o => {
        if (user_o)
          resolve(new UserData().setOrigin(user_o));
        else
          reject();
      }, reject)
  });
}

export const COLLAB_CORRECT     = 0;
export const COLLAB_ERROR_EXIST = 1;
export const COLLAB_ERROR_SELF  = 2;

export function checkCollaboration(user) {
  if (!user)
    return COLLAB_ERROR_EXIST;
  
  if (user.username == Parse.User.current().get('username'))
    return COLLAB_ERROR_SELF;
  
  let collabs = store.getState().models.currentSite.collaborations;
  for (let collab of collabs) {
    if (collab.user.origin.id == user.origin.id)
      return COLLAB_ERROR_EXIST;
  }
  
  return COLLAB_CORRECT;
}

export function getContentForModel(model) {
  let items = store.getState().content.items;
  let res = [];
  for (let item of items) {
    if (item.model == model)
      res.push(item);
  }
  return res;
}

export function getModelByName(name) {
  for (let model of store.getState().models.models) {
    if (model.name == name)
      return model;
  }
  return null;
}

export function getMediaByO(origin) {
  if (!origin)
    return null;

  let items = store.getState().media.items;
  for (let item of items) {
    if (item.origin && item.origin.id == origin.id)
      return item;
  }
  return null;
}

export function getContentByO(origin, items) {
  if (!origin)
    return null;
  
  if (!items)
    items = store.getState().content.items;
  for (let item of items) {
    if (item.origin && item.origin.id == origin.id)
      return item;
  }
  
  return null;
}

export function getSiteByO(origin) {
  if (!origin)
    return null;
  
  for (let site of store.getState().models.sites) {
    if (site.origin && site.origin.id == origin.id)
      return site;
  }
  return null;
}