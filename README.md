Chisel
=====================

> A Parse CMS

## Build Setup

First, you should get a [Parse local server](https://github.com/Nes-si/chisel-parse-server).

After running that, you can work with this app:

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:3000
npm start

# build for production with minification
npm run build
```

Or you can connect to remote server, if you edit src/ducks/initialize.js.